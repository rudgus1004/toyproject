<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<h1 align="center">회원가입</h1>
<hr />
<form action="/join2" method="post">
<div align="center">
<div class="form-group" style="width: 30%">
<label for="">아이디 입력</label>
<input type="text"  class="form-control" name="userId"/>
</div>
<br />
<div class="form-group" style="width: 30%">
<label for="">비밀번호 입력</label>
<input type="password"  class="form-control" name="userPwd"/>
</div>
<br />
<div class="form-group" style="width: 30%">
<label for="">회원명</label>
<input type="text" class="form-control" name="userName"/>
</div>
<br />
<div class="form-group" style="width: 30%">
<label for="">이메일</label>
<input type="email" class="form-control" name="email"/>
</div>
<br />
<input type="submit" value="회원가입"  class="btn btn-default"/>
</div>

</form>

</body>
</html>