package com.kh.toyproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.kh.toyproject.model.service.MemberSerivce;
import com.kh.toyproject.model.vo.Member;

@Controller
public class Main {
	
	@Autowired
	private MemberSerivce ms;
	
	@GetMapping("/index")
	public void TestMain(Model model) {
		System.out.println("한번 해봅시다ㅏ");
		

		
		
	}
	
	@GetMapping("Member/join")
	public void joinPage() {
		
	}
	
	@PostMapping("/join2")
	public ModelAndView joinPage2(ModelAndView mv, Member mb) {
		
		mb.setDivision("일반회원");
		
		System.out.println("mb 값 확인 : " + mb);
		
		ms.joinsave(mb);
		
		mv.addObject("msg", "회원가입이 완료 되었습니다.");
		mv.setViewName("/index");
		
		return mv;
		
		
		
	}
	
	@PostMapping("/Member/join3")
	public void login(Model model , Member mb) {
		
		System.out.println("잘들어오냐 ? :" + mb);
		
		Member member = ms.login(mb.getUserId(), mb.getUserPwd());
		
		System.out.println("list 값 : " + member);
		
		model.addAttribute("member" , member);
		
		
	}

}
