package com.kh.toyproject.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kh.toyproject.model.dao.MemberDao;
import com.kh.toyproject.model.vo.Member;

@Service
public class MemberSerivce {
	
	@Autowired
	private MemberDao md;
	
	public Member login(String userId,String userPwd) {
		
		return md.login(userId, userPwd);
	}

	public void joinsave(Member mb) {

		md.save(mb);
	}

}
