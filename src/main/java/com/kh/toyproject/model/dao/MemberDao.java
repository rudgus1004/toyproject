package com.kh.toyproject.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.kh.toyproject.model.vo.Member;

@Repository
public interface MemberDao extends CrudRepository<Member, String>{

	@Query(nativeQuery = true , value="SELECT * FROM USERS" )
		List<Member> findAll();
	
	@Query(nativeQuery = true , value="SELECT * FROM USERS WHERE USER_ID = :userId AND USER_PWD = :userPwd" )
	Member login(@Param("userId") String userId , @Param("userPwd")String userPwd);
	
}
