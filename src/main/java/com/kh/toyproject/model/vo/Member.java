package com.kh.toyproject.model.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@Data
@ToString
@Entity
@SequenceGenerator(
		name="USER_NO_SEQ",
		 sequenceName="USER_NO", 
		 initialValue=1,
	  allocationSize=1 
		)

@Table(name="USERS")
public class Member {
	@Id
	@GeneratedValue(
            strategy=GenerationType.SEQUENCE,
            generator="USER_NO_SEQ"    
            )
	@Column(name="USER_NO")
	private int userNo;
	@Column(name="USER_ID")
	private String userId;
	@Column(name="USER_PWD")
	private String userPwd;
	@Column(name="USER_NAME")
	private String userName;
	@Column(name="EMAIL")
	private String email;
	@Column(name="DIVISION")
	private String division;
//	@Column(name="PHONE")
//	private String phone;
//	@Column(name="ENROLL_DATE")
//	private Date enrollDate;
//	@Column(name="DELETE_DATE")
//	private Date deleteDate;
//	@Column(name="CLASSIFICATION")
//	private String classification;
//	@Column(name="DELETE_REASON")
//	private String deleteReason;
//	@Column(name="STATUS")
//	private String status;
	
	
}
