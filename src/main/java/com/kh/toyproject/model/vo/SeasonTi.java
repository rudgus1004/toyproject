package com.kh.toyproject.model.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Data
@ToString
@Entity
@Table(name="SEASON_TI")
public class SeasonTi {
	
	@Id
	@Column(name="SEASON_ID")
	private int seasonId;
	@Column(name="SEASON_NAME")
	private String seasonName;
	@Column(name="SEASON_DATE")
	private String seasonDate;
	@Column(name="SEASON_MONEY")
	private String seasonMoney;
	@Column(name="SEASON_DC")
	private String seasonDc;
	
}
